var apiBenchmark = require('api-benchmark');
var fs = require("fs");
var path = require("path");
var globals = require("./globals");
const login = require('./fixture/login');
// Or for from data login use this file
// const login = require('./fixture/loginFormdata');

var service = {
    server1: 'https://my:server/'
};

function benchmark(routes) {
    apiBenchmark.measure(service, routes, function (err, results) {
        apiBenchmark.getHtml(results, function (error, html) {

            let report = path.resolve(__dirname, "./reports/index/" + globals.getCurrentTimestamp() + "-index.html");
            fs.writeFile(report, html, (err) => {
                if (err) {
                    return console.log(err);
                }
                console.log('\x1b[36m%s\x1b[0m', "Report for Index is generated!");
            });

        });
    });
}

async function test() {
    const accessTokenData = await login({ email: process.env.USERNAME, password: process.env.PASSWORD })
    const accessToken = accessTokenData.token

//Enter your endpoints here
    var routes = {
        GetMyProfile: {
            method: 'get',
            route: '/my:path/',
            headers: {
                Authorization: accessToken
            }
        },
    
        GetOtherProfile: {
            method: 'get',
            route: '/my:path/1',
            headers: {
                Authorization: accessToken
            }
        },
    };
    benchmark(routes)
}


test()