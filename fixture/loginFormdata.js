
const assert = require('assert');
const axios = require('axios');
const formurlencoded = require('form-urlencoded').default;

const loginUrl = "https://my:server/my:login:path"


// Header if needed
// const axiosConfig = {
//     headers: {
//         "Authorization": "token..."
//     }
// }


module.exports = async function login({ email, password }) {
    try {

// Your form data object
        const obj = {
            username: email,
            password,
            grant_type: 'password'
        }
        const config = axiosConfig ? axiosConfig : null
        const { data } = await axios.post(loginUrl, (formurlencoded(obj)), config)
        const token = data.access_token;

        assert.ok(token, 'Login Failed')

        return {
            token: "Bearer" + " " + token
        }
    } catch (e) {
        console.log('error: ', e);

    }
}
