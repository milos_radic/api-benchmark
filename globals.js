	const moment = require('moment')
	

	function getCurrentTimestamp(){
		return moment().format('YYYY-MM-DDTHH_mm_ss')
	}

module.exports = {
	getCurrentTimestamp
}