const assert = require('assert');
const axios = require('axios');

const loginUrl = "https://my:server/my:login:path"

module.exports = async function login({ email, password }) {
    try {
        const { data } = await axios.post(loginUrl, { email, password })
        const token = data.access_token;

        assert.ok(token, 'Login Failed')

        return {
            token: "Bearer" + " " + token
        }
    } catch(e){
        console.log("error: ", e);
    }
}
